class BugsController < ApplicationController
  before_action :set_bug, only: [:show, :edit, :update, :destroy]
  protect_from_forgery unless: -> { request.format.json? }
  before_filter :cors_set_access_control_headers

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = '*'
    headers['Access-Control-Allow-Headers'] = '*'
    headers['Access-Control-Request-Method'] = '*'
  end

  respond_to :html, :json

  def index
    @bugs = Bug.all

    respond_to do |format|
      format.html { respond_with(@bugs) }
      format.json {
        render json: @bugs, :callback => 'callback'
      }
    end

  end

  def show
    respond_with(@bug)
  end

  def new
    @bug = Bug.new
    respond_with(@bug)
  end

  def edit
  end

  def create
    @bug = Bug.new(bug_params)
    @bug.save
    respond_with(@bug)
  end

  def update
    @bug.update(bug_params)
    respond_with(@bug)
  end

  def destroy
    @bug.destroy
    respond_with(@bug)
  end

  private
    def set_bug
      @bug = Bug.find(params[:id])
    end

    def bug_params
      params.require(:bug).permit(:name, :description, :project_id, :state_id)
    end
end
