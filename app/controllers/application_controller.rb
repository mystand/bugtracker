class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  before_filter :authenticate_or_token

  def options
    head :status => 200, :'Access-Control-Allow-Headers' => 'accept, content-type'
  end

private

  def authenticate_or_token
    if params[:api_key] == "6a4f2c952b3ccbc8924b89b7f5c3d0dc"
      @current_user = User.first
      return current_user
    end
    authenticate_user!
  end

end