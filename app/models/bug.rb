class Bug < ActiveRecord::Base
  belongs_to :state
  belongs_to :project

  validates :name, :project_id, :state_id,
            :presence => true
end