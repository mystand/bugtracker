class CreateBugs < ActiveRecord::Migration
  def change
    create_table :bugs do |t|
      t.string :name
      t.text :description
      t.integer :project_id
      t.integer :state_id

      t.timestamps null: false
    end
  end
end
